import com.github.jengelman.gradle.plugins.shadow.ShadowExtension
import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar
import org.apache.tools.ant.filters.ReplaceTokens
import org.gradle.api.JavaVersion.VERSION_1_8
import java.net.URI
import org.apache.tools.ant.filters.*
import org.gradle.plugins.ide.idea.model.IdeaLanguageLevel

plugins {
    `java-gradle-plugin`
    `java-library`
    kotlin("jvm") version "1.2.50"
    id("com.github.johnrengelman.shadow") version "2.0.4"
    idea
    maven
}

group = "com.gitlab.martijn-heil.ninbukkitflyway"
version = "1.0-SNAPSHOT"
description = "NinBukkitFlyway"

apply {
    plugin("java")
    plugin("kotlin")
    plugin("com.github.johnrengelman.shadow")
    plugin("java-library")
    plugin("idea")
    plugin("maven")
}

java {
    sourceCompatibility = VERSION_1_8
    targetCompatibility = VERSION_1_8
}

tasks {
    withType<ProcessResources> {
        filter(mapOf(Pair("tokens", mapOf(Pair("version", version)))), ReplaceTokens::class.java)
    }
    withType<ShadowJar> {
        this.classifier = null
        this.configurations = listOf(project.configurations.shadow)
    }
}

tasks["build"].dependsOn("shadowJar")
defaultTasks = listOf("build")

repositories {
    maven { url = URI("https://hub.spigotmc.org/nexus/content/repositories/snapshots/") }
    maven { url = URI("https://jitpack.io")  }

    mavenCentral()
    mavenLocal()
}

idea {
    project {
        languageLevel = IdeaLanguageLevel("1.8")
    }
    module {
        isDownloadJavadoc = true
        isDownloadSources = true
    }
}

dependencies {
    api("org.bukkit:bukkit:1.12.2-R0.1-SNAPSHOT") { isChanging = true }
    shadow(fileTree("lib") { include("*.jar") })
    shadow(kotlin("stdlib"))
    shadow("com.gitlab.martijn-heil:Flyway:-SNAPSHOT") { isChanging = true }
    api("com.gitlab.martijn-heil:Flyway:-SNAPSHOT") { isChanging = true }
}
