# NinBukkitFlyway
Bukkit plugin which simply packages Flyway to bring it into the classpath of a Bukkit server, sort of like a dynamic library.

Last updated for Bukkit `1.12.2-R0.1-SNAPSHOT`.