rootProject.name = "NinBukkitFlyway"

pluginManagement {
    repositories {
        gradlePluginPortal()
        jcenter()
        mavenCentral()
        mavenLocal()
    }
}
